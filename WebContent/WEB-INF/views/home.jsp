<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>

	<form:form action="submitForm" modelAttribute="io">
		<table border="1">
        	<tr>
            				<td>Select Language :
            				<form:select path="language">
                                    <form:option value="javascript">Javascript</form:option>
                                    <form:option value="r" >R</form:option>
                                    <form:option value="groovy">Groovy</form:option>
                                    <form:option value="python">Python</form:option>
                                    <form:option value="scala">Scala</form:option>
                                    <form:option value="kotlin">Kotlin</form:option>
                                    <form:option value="clojure">Clojure</form:option>
                                 </form:select>
                                  </td>
            				<td><input type="submit" value="Run"/> </td>
            			</tr>

        		<tr>
        			<th>Script</th>
        			<th>Output</th>

        		   </tr>
        		   <tr>
                 				<td><form:textarea path="script" rows="15" cols="100" /></td>

                 				<td><form:textarea path="result" rows="15" cols="50" /></td>


                 			</tr>
                 			<tr>

                                    				<td colspan="2">Errors</td>
                                    				</tr>
                        <tr>

        				<td colspan="2"><form:textarea path="errorDetails" rows="15" cols="155" /></td>

        			</tr>

        	</table>
	</form:form>
</body>
</html>