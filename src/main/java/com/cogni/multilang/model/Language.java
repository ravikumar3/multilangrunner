package com.cogni.multilang.model;

public enum Language {

    JAVASCIPT("javascript"),
    R("r"),
    GROOVY("groovy"),
    PYTHON("python"),
    SCALA("scala"),
    KOTLIN("kotlin"),
    CLOJURE("clojure");

    private String lang;

    Language(String lang) {
        this.lang = lang;
    }

    public String getLang() {
        return lang;
    }

    public static void main(String[] args) {
        for(Language env : Language.values())
        {
            System.out.println(env + " :: "+  env.name() + " :: "+ env.getLang());
        }
    }
}