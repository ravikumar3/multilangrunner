package com.cogni.multilang.model;

public class InputOutput {

    String language ;
    String script;
    String result;
    String errorDetails;

    public InputOutput(String language, String script, String result, String errorDetails) {
        this.language = language;
        this.script = script;
        this.result = result;
        this.errorDetails = errorDetails;
    }

    public InputOutput() {
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }
}
