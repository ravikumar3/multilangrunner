package com.cogni.multilang.controller;

import java.util.Locale;

import com.cogni.multilang.model.InputOutput;
import com.cogni.multilang.service.MultiLangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	@Autowired
	MultiLangService mlService;

	@RequestMapping(value = "/")
	public String home(Locale locale, Model model) {
		InputOutput io = new InputOutput("javascript", "", "", "");
		model.addAttribute("io", io);
		return "home";
	}

	@RequestMapping("/submitForm")
	public String getAllEmployees(@ModelAttribute("reservation") InputOutput io, Model model)
	{
		System.out.println("Request before..................");
		System.out.println(io.getLanguage());
		System.out.println(io.getScript());
		System.out.println(io.getResult());
		System.out.println(io.getErrorDetails());

		io = mlService.runScript(io);
		System.out.println("Request After..................");
		System.out.println(io.getLanguage());
		System.out.println(io.getScript());
		System.out.println(io.getResult());
		System.out.println(io.getErrorDetails());

		model.addAttribute("io", io);
		return "home";
	}
}
