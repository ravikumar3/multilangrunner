package com.cogni.multilang;
import com.cogni.multilang.model.Input;
import com.cogni.multilang.model.Output;
import com.cogni.multilang.util.LoggerStackTraceUtil;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.StringWriter;

/**
 * Hello world!
 */
public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello World!");
        Input input = new Input();
        input.setLanguage("javascript");
        input.setScript("var x = 100;\n" +
                "var y = 20;\n" +
                "var z = x + y;\n" +
                "print (z);");
        Output out = run(input);
        System.out.println(out.getResult()+"+++++++++++++++++++++++=\n"+out.getErrorDetails()+"\n+++++++++++++++++++++");
    }

    // DO NOT USE THIS STYLE -- INTENDED ONLY AS QUICK AND DIRTY HACKS..
    public static Output run(Input input) {
        System.out.println("Lang: " + input.getLanguage());
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName(input.getLanguage());
        Output out = new Output();
        try {
            engine.eval(input.getScript());
        } catch (ScriptException e) {
            LoggerStackTraceUtil l = new LoggerStackTraceUtil(e);
            out.setErrorDetails(l.getErrorMessage());
        }

        // Capture output to
        StringWriter outputWriter = new StringWriter();
        engine.getContext().setWriter(outputWriter);

        out.setResult(outputWriter.toString());
        // Reset output to console
        // engine.getContext().setWriter(new PrintWriter(System.out));
        return out;
    }

    public static void supportingLanguages() {
        System.out.println("***** Dumping Factory/Engine Information");
        ScriptEngineManager manager = new ScriptEngineManager();
        for (ScriptEngineFactory t : manager.getEngineFactories()) {
            System.out.println(t.getEngineName() + "," + t.getLanguageName() + "," + t.getLanguageVersion());
        }
    }
   /* // DO NOT USE THIS STYLE -- INTENDED ONLY AS QUICK AND DIRTY HACKS..
    public static void runJavaScript() throws Exception {
        System.out.println("javascript");
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        engine.eval("var x = 10;");
        engine.eval("var y = 20;");
        engine.eval("var z = x + y;");
        engine.eval("print (z);");
    }

    public static void runR() throws Exception {
        System.out.println("R Language");
        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine();
        // run whatever R Code is here.....
        engine.eval("df <- data.frame(x=1:10, y=(1:10)+rnorm(n=10))");
        engine.eval("print(df)");
        engine.eval("print(lm(y ~ x, df))");
    }

    public static void passingJavaParamstoR() throws Exception {
        System.out.println("R Language");
        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine();
        engine.put("x", 4);
        engine.put("y", new double[]{1d, 2d, 3d, 4d});
        engine.put("z", new DoubleArrayVector(1, 2, 3, 4, 5));
        engine.put("hashMap", new java.util.HashMap());
// some R magic to print all objects and their class with a for-loop:
        engine.eval("for (obj in ls()) { " +
                "cmd <- parse(text = paste('typeof(', obj, ')', sep = ''));" +
                "cat('type of ', obj, ' is ', eval(cmd), '\\n', sep = '') }");
    }

    public static void passingJavaParamstoR2() throws Exception // create a string vector in R:
    {
        RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
        ScriptEngine engine = factory.getScriptEngine();

        Vector x = (Vector) engine.eval("c(\"foo\", \"bar\")");

        double x1 = new Double(x.get(0).toString());
        if (Double.isNaN(x1)) {
            System.out.println("Result is NaN");
        }
        String s = x.get(0).toString();
        System.out.println("First element of result is " + s);
// call the toString() method of the underlying StringArrayVector:
        System.out.println("Vector as defined in R: " + x);
    }

    public static void runGroovy() throws Exception {
        System.out.println("Groovy");
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("groovy");
        if (engine == null) System.out.println("groovy engine is null");
        Integer sum = (Integer) engine.eval("(1..10).sum()");
        System.out.println("From Groovy: Sum is: " + sum.toString());
    }

    public static void runClojure() throws Exception {
        System.out.println("Clojure");
        IFn plus = Clojure.var("clojure.core", "+");
        System.out.println("From Clojure: " + plus.invoke(1, 2));
    }

    public static void runRuby() throws Exception {
        System.out.println("Ruby");
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("jruby");
        ScriptContext context = engine.getContext();
        context.setAttribute("label", new Integer(4), ScriptContext.ENGINE_SCOPE);
        engine.eval("puts \"From Ruby : #{2+$label}\"", context);
    }

    public static void runPython() throws Exception {
        System.out.println("Python");
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("python");
        System.out.println("The following are from Python");
        engine.eval("import sys");
        engine.eval("print sys");
        engine.eval("print (sys.version)");
        engine.put("a", 42);
        engine.eval("print a");
        engine.eval("x = 2 + 2 + a");
        Object x = engine.get("x");
        System.out.println("x: " + x);
    }

    public static void runKotlin() throws Exception {
        System.out.println("Kotlin");
        ScriptEngineManager factory = new ScriptEngineManager();
        //ScriptEngine engine = factory.getEngineByExtension("kts"); // another hack...
        ScriptEngine engine = factory.getEngineByName("kotlin");
        try {
            System.out.println(engine.eval("3 + 5"));
            engine.eval("val c = 'a'; val ascii = c.toInt();");
            engine.eval("println(\"From Kotlin: The ASCII value of $c is: $ascii\")");
        } catch (Exception e) {
            //e.printStackTrace();
            LoggerStackTraceUtil.printErrorMessage(e);
            throw e;
        }
    }

    public static void runScala() throws Exception {
        System.out.println("Scala");
        ScriptEngineManager factory = new ScriptEngineManager();
        ScriptEngine engine = factory.getEngineByName("scala");
        ((BooleanSetting) (((IMain) engine).settings().usejavacp())).value_$eq(true);
        //engine.getContext().setAttribute("label", new Integer(4), ScriptContext.ENGINE_SCOPE);
        engine.getBindings(ScriptContext.ENGINE_SCOPE).put("n", new Integer(15));
        engine.getBindings(ScriptContext.ENGINE_SCOPE).put("label", new Integer(15));
        engine.getBindings(ScriptContext.ENGINE_SCOPE).put("label1", new Integer(15));

        //engine.getContext().setAttribute("label1", new Integer(15), ScriptContext.ENGINE_SCOPE);
        engine.eval("println(2 + label.asInstanceOf[Int]);");
        engine.eval("1 to 10 foreach { print(_) }; println; ");
        engine.eval("1 to label1.asInstanceOf[Int] foreach { print(_) }; println;");
    }*/
}
