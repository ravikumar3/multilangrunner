package com.cogni.multilang.service;

import com.cogni.multilang.model.InputOutput;
import com.cogni.multilang.util.LoggerStackTraceUtil;
import org.renjin.parser.ParseException;
import org.renjin.script.RenjinScriptEngineFactory;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.PrintWriter;
import java.io.StringWriter;

@Service
public class MultiLangServiceImpl implements MultiLangService {


    @Override
    public InputOutput runScript(InputOutput io) {
        // InputOutput i = new InputOutput();
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = null;
        if ("r".equals(io.getLanguage())) {

            RenjinScriptEngineFactory factory = new RenjinScriptEngineFactory();
            engine = factory.getScriptEngine();
        } else {
            engine = manager.getEngineByName(io.getLanguage());
        }

        StringWriter outputWriter;
        try {
            outputWriter = new StringWriter();
            engine.getContext().setWriter(outputWriter);
            engine.eval(io.getScript());
            String output = outputWriter.toString();
            engine.getContext().setWriter(new PrintWriter(System.out));
            io.setResult(outputWriter.toString());
        } catch (ScriptException e) {
            LoggerStackTraceUtil l = new LoggerStackTraceUtil(e);
            io.setErrorDetails(l.getErrorMessage());

        } catch (ParseException e){
            LoggerStackTraceUtil l = new LoggerStackTraceUtil(e);
            io.setErrorDetails(l.getErrorMessage());
        }

        // Capture output to

        System.out.println("In IMPL..................");
        System.out.println(io.getLanguage());
        System.out.println(io.getScript());
        System.out.println(io.getResult());
        System.out.println(io.getErrorDetails());


        // Reset output to console
        // engine.getContext().setWriter(new PrintWriter(System.out));
        return io;
    }

    public static void main(String[] args) throws Exception {
        MultiLangServiceImpl m = new MultiLangServiceImpl();
        System.out.println("Hello World!");
        InputOutput io = new InputOutput();
        io.setLanguage("javascript");
        io.setScript("var x = 10;\n" +
                "var y = 20;\n" +
                "var z = x + y;\n" +
                "print (z);");
        m.runScript(io);
        //System.out.println(io.getResult()+"+++++++++++++++++++++++=\n"+io.getErrorDetails()+"\n+++++++++++++++++++++");
    }
}
