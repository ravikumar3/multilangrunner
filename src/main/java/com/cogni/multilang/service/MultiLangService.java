package com.cogni.multilang.service;

import com.cogni.multilang.model.InputOutput;

public interface MultiLangService
{
	public InputOutput runScript(InputOutput input);
}
